Many times you need to implement hooks to alter the behaviour of an existing module. 
One option would be to open the module file and alter the behaviour. However this approach 
is not upgrade proof i.e. when you upgrade the module, the code is lost. The other 
alternative is to write a new module everytime to want to change the behaviour.

This module provides a generic implementation of the most used hooks. In order to change the 
behaviour you need to write the custom code and let the Quick Hooks module load it for you. 
This will potentially reduce the number of modules that you need to enable on your site.
